import { useNavigate } from "react-router-dom";
import { useState} from "react";
import { Link } from 'react-router-dom'
import './Login.scss'



export default function Register() {
    const navigate = useNavigate();
    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")

    const handleRegister = async () =>{
        if (email === ""){
            alert("Chưa nhập email!")
        }
        if (username === ""){
            alert("Chưa nhập tên tài khoản!")
        }
        if( password === ""){
            alert("Chưa nhập mật khẩu!")
        }
        else{
            try{
                const res = await fetch("https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/user/Login",{
                    method: "PUT",
                    headers: {"Content-Type": "application/json",},
                    body: JSON.stringify({
                        Email: email,
                        Name: username,
                        Password: password,
                    }),
                });
                const json = await res.json();
                localStorage.setItem("authentication", JSON.stringify(json));
                navigate('/')

            }
            catch(error){
                alert("Thông tin không hợp lệ, mời nhập lại!")
            }
        }
    }

  return (
    <div  className='main'>
        <div className='form'>
            <div className=' logo'  >
            <Link to={'/'} >
                <img  src='https://www.galaxycine.vn/website/images/galaxy-logo.png' alt='logo' /></Link>
            </div>
            <div className=' mainForm' >
                <span className=' title' >Đăng Ký</span>
                <div className=' inputForm' >
                    <input className=' input1'  type="text" name='email' placeholder='Email'  value={email} onChange={(e) => setEmail(e.target.value)}  />
                    
                </div>
                <div className=' inputForm' >
                    <input className='input1'  type="text"  name='username' placeholder='Tên tài khoản' value={username} onChange={(e) => setUsername(e.target.value)}  />
                    
                </div>
                <div className=' inputForm' >
                    <input className='input1'  type="password" name=' pass' placeholder='Password'  value={password} onChange={(e) => setPassword(e.target.value)}  />            
                </div>
                <div className=' container-btn' >
                    <Link to={'/login'} >
                    <button className=' loginBtn' onClick={handleRegister}>Đăng Ký </button></Link>
                </div>
            </div>
        </div>
    </div>
  )
}
