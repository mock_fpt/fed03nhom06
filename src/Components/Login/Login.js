import { useNavigate } from "react-router-dom";
import { useState} from "react";
import { Link } from 'react-router-dom'
import './Login.scss'



export default function Login() {
    const navigate = useNavigate();
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const handleLogin = async () =>{
        if (username === ""){
            alert("Chưa nhập tên tài khoản!")
        }
        if( password === ""){
            alert("Chưa nhập mật khẩu!")
        }
        else{
            try{
                const res = await fetch("https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/user/Login",{
                    method: "POST",
                    headers: {"Content-Type": "application/json",},
                    body: JSON.stringify({
                        Email: username,
                        Password: password,
                    }),
                });
                const json = await res.json();
                localStorage.setItem("authentication", JSON.stringify(json));
                navigate('/')

            }
            catch(error){
                alert("Sai thông tin đăng nhập, mời nhập lại!")
            }
        }
    }
  return (
    <div  className='main'>
        <div className='form'>
            <div className=' logo'  >
            <Link to={'/'} >
                <img  src='https://www.galaxycine.vn/website/images/galaxy-logo.png' alt='logo' /></Link>
            </div>
            <div className=' mainForm' >
                <span className=' title' >Đăng Nhập</span>
                <div className=' inputForm' >
                    <input className=' input1'  type="text"  name=' username'  placeholder='email' value={username} onChange={(e) => setUsername(e.target.value)}  />
                    
                </div>
                <div className=' inputForm' >
                    <input className='input1'  type="password" name=' pass'  placeholder=' Mật khẩu' value={password} onChange={(e) => setPassword(e.target.value)}  />
                    
                </div>
                <div className=' container-btn' >
                    <button className=' loginBtn' onClick={()=> handleLogin()} >Đăng Nhập </button>
                </div>
                <div className=' text-center' >
                    <span className=' txt1' >Forgot </span>
                    <a className=' txt2'  href=' #' >Tên đăng nhập / Mật khẩu?</a>
                </div>
                <div className=' text-center' >
                    <Link to={'/register'} >Tạo tài khoản</Link>
                </div>
            </div>
        </div>
    </div>
  )
}
