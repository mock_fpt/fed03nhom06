import { Link, Outlet } from 'react-router-dom';
import Carou from './BCarou/Carou';
import './Body.scss'
import { useState } from 'react';
import ManuFilm from './ManuFilm/ManuFilm';
import News from '../CineNews/News';

function Body() {
    const [Show, setShow] = useState(true)
    const showMovie = (isShow)=>{
        setShow(isShow);
    }
    return (
        <div >
            <Carou/>
            <div className='Main' >
                <div className='navi'>
                    <Link to={'/movieShowing'}><span onClick={()=>showMovie(false)}><h3 className="Now" >Now On</h3></span></Link>
                    <Link to={'/movieComming'}><span onClick={()=>showMovie(false)}><h3 className="Soon" >Comming Soon</h3></span></Link>
                    <Outlet/>               
                </div>
                <div style={{ display: (Show ? "flex" : "none") }}>
                    <ManuFilm />
                    <div onClick={()=>showMovie(false)} >
                        
                    </div>
                </div>
            </div>
            
        </div>
    )
}
export default Body;