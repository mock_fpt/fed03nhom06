
import React, { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom'
import Carousel from 'react-bootstrap/Carousel';
import './carousc.scss'


function Carou(prop) {
    const [Mrap, setLsFilm] = useState([])
    const nav = useNavigate()
    useEffect(() => {
        fetch("https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/nowAndSoon")
            .then(res => res.json()
                .then(data =>
                    setLsFilm(data)
                ))
                
    }, [])
    const ShowFilm = (id) => {
        nav('/' + id)
    }

    return (
     <div>
        <Carousel className='Container'>
        {
            Mrap.movieShowing?.map((n,i) => {
                return  <Carousel.Item interval={2500} onClick={() => ShowFilm(n.id)} key={i}>
                    <img  className='imgMobile' src={n.imageLandscapeMobile} width='100%' />
                </Carousel.Item>
            })
        }
        </Carousel>
    </div>
  )}

export default Carou

