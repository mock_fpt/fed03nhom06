import React, { useEffect, useState } from "react";
import { Link, useNavigate } from 'react-router-dom'
import './ManuFilm.scss'


function ManuFilm2() {
    const [Mrap, setlsFilm] = useState([])
    const nav = useNavigate()
    useEffect(() => {
        fetch("https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/nowAndSoon")
            .then(res => res.json()
                .then(data =>
                    setlsFilm(data)
                ))
    }, [])



    const ShowFilm = (id) => {
        nav('/' + id)
    }
    return (
        <div className='BoxB'>
            <div className='banner'></div>
            <div className='TableLs'>
                <div className='FilmMenu'>
                    {
                        Mrap.movieCommingSoon?.map((v, i) => {
                            return <div className='AImage' key={i}  >
                                <div className='BImage' onClick={() => ShowFilm(v.id)} >
                                    <img src={v.imageLandscape} width={390} alt='img' />
                                </div>
                                <p className="title">{v.name}</p>
                            </div>
                        })
                    }
                </div>
            </div>
        </div>
    )
}


export default ManuFilm2