import React, { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom'
import './ManuFilm.scss'


function ManuFilm() {
    const [Mrap, setlsFilm] = useState([])
    const nav = useNavigate()
    useEffect(() => {
        fetch("https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/nowAndSoon")
            .then(res => res.json()
                .then(data =>
                    setlsFilm(data)
                ))
    }, [])



    const ShowFilm = (id) => {
        nav('/' + id)
    }
    return (
        <div className='BoxB'>
            <div className='TableLs'>
                <div className='FilmMenu'>
                    {
                        Mrap.movieShowing?.map((v, i) => {
                            return <div className='AImage' key={i}  >
                                <div className='BImage' onClick={() => ShowFilm(v.id)} >
                                    <img src={v.imageLandscape} width={390} alt='img' />
                                </div>
                                <p className="title">{v.name}</p>
                            </div>
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default ManuFilm