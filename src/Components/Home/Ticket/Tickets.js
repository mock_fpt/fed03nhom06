import React, { useState } from 'react'
import './Tickets.scss'
import { Link, Outlet } from 'react-router-dom';
import Footer from '../Footer/Footer'
import Header from '../Header/Header'
import TicketByMovie from './TicketByMovie'

export default function Tickets() {
  const [Show, setShow] = useState(true)
    const showMovie = (isShow)=>{
        setShow(isShow);
    }
  return (
    <div>
        <Header/>
        <div className='Main' >
            <div className='navi'>
                <Link to={'/tickets/TicketByMovie'}><span onClick={()=>showMovie(false)}>THEO PHIM</span></Link>
                <Link to={'/tickets/TicketByCinema'}><span onClick={()=>showMovie(false)}>THEO RẠP</span></Link>
                <Outlet/>               
            </div>
            <div style={{ display: (Show ? "flex" : "none") }}>
                <TicketByMovie />
                <div onClick={()=>showMovie(false)} >

                </div>
            </div>
        </div>
        <Footer/>
    </div>
  )
}
   
