import React, {useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import './TicketByCinema.scss'


export default function TicketByCinema() {
  const navigate = useNavigate();
  //const { slug } = useParams
  const [Cinema, setCinema] = useState([])
  const [movies, setMovies] = useState([])
  //const [showtime, setShowTime] = useState([])
  useEffect(() => {
    fetch("https://vietcpq.name.vn/U2FsdGVkX18VmUfb3R34rkA11q59f+dY6kr6oyXqCbg=/cinema/cinemas")
        .then(res => res.json())
        .then(data => {
           setCinema(data)
        });
      }, []);
  const getFilm = (idCinema)=>{
    fetch(`https://vietcpq.name.vn/U2FsdGVkX19udsrsAUnUBsRg8K4HmweHVb4TTgSilDI=/cinema/cinemas/${idCinema}`)
        .then(res => res.json())
        .then(data => {
            setMovies(data)
        })
      }

 // const getShowTime = (slug) => {
 //     let result = Cinema.find(k=>k.slug===slug)
  //    setShowTime(result)
  //  }

  const handleBooking = (Cinema) =>{
    navigate(`/login/bookingandticket/${Cinema.id}`)
  }
  {

    return (
      <div>
          <div className='tickets'>
              <div className='ticket_body'>
                <div className='cinema'>
                  <div className='title'>
                      <h4>CHỌN RẠP</h4>
                  </div>
                  <div className='items'>
                    <ul>
                      {
                        Cinema.map((n,i)=>{
                          return <li key={i} onClick={()=>{getFilm(n.code)}}>
                            <span>{n.name}</span>
                          </li>
                        })
                      }
                    </ul>
                  </div>
                </div>
                <div className='movies'>
                  <div className='title'>
                      <h4>CHỌN PHIM</h4>   
                  </div>
                  <div className='items'>
                    <ul>
                      {
                        movies.map((n,i)=>{
                          return <li key={i} >
                            <img src={n.imageLandscape} alt={n.slug} width={100} /* onClick={() => { getShowTime(n.slug) }}*/ />
                            <span>{n.name}</span>
                          </li>
                        })
                      }
                    </ul>
                  </div>
                </div>
                <div className='showtime'>
                  <div className='title'>
                      <h4>CHỌN SUẤT</h4>
                  </div>
                  <div className='items'>
                      <div>
                        {
                          movies.map((n,i)=>{
                            return <div key={i}>
                              {
                                 n.dates.map((d, i2) => {
                                  return <div key={i2} className="show">
                                    <h4>- {d.showDate}</h4>
                                     <div className='time'>
                                      {
                                        d.bundles.map((b) => {
                                          return <div>
                                            <h5>Ticket {b.version}</h5>
                                            <div>
                                              {
                                                b.sessions.map(t => {
                                                  return <span style={{border:'1px solid black'}} onClick={() => handleBooking(Cinema)}>{t.showTime} </span>
                                                })
                                              }
                                            </div>
                                          </div>
                                          })
                                      }
                                      </div>
                                    </div>
                                  })
                                }
                            </div>
                          })
                        }
                      </div>
                  </div>
                </div>
              </div>
          </div>
      </div>
    )
  }
}
