import React, { useEffect, useState } from 'react'
import { useNavigate} from 'react-router-dom'
import './TicketByMovie.scss'

export default function TicketByMovie() {
    const navigate = useNavigate();
    const [Cinema, setCinema] = useState([])
    const [movies, setMovies] = useState([])
    const [showtime, setShowTime] = useState([])
    useEffect(() => {
        fetch("https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/nowAndSoon")
            .then(res => res.json())
            .then(data => {
                setMovies(data)
            })
    })
    const getCinema = (idMovies) => {
        fetch(`https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/movie/${idMovies}`)
            .then(res => res.json())
            .then(data => {
                setCinema(data)
            })
    }
    const getShowTime = (idCinema) => {
        fetch(`https://vietcpq.name.vn/U2FsdGVkX19udsrsAUnUBsRg8K4HmweHVb4TTgSilDI=/cinema/cinemas/${idCinema}`)
            .then(res => res.json())
            .then(data => {
                setShowTime(data)
            });
    }
    const handleBooking = (Cinema) => {
        navigate(`/login/bookingandticket/${Cinema.id}`)
    }
    {
        return (
            <div className='tickets'>
                <div className='ticket_body'>

                    <div className='movies'>
                        <div className='title'>
                            <h4>CHỌN PHIM</h4>
                        </div>
                        <div className='items'>
                            <ul>
                                {
                                    movies.movieShowing?.map((n, i) => {
                                        return <li key={i} onClick={() => { getCinema(n.id) }}  >
                                            <img src={n.imageLandscape} alt={n.slug} width={100} />
                                            <span>{n.name}</span>
                                        </li>
                                    })
                                }
                            </ul>
                        </div>

                    </div>
                    <div className='cinema'>
                        <div className='title'>
                            <h4>CHỌN RẠP</h4>
                        </div>
                        <div className='items'>
                            <ul>
                                {
                                    Cinema.map((n, i) => {
                                        return <li key={i} onClick={() => { getShowTime(n.code) }} >
                                            <span>{n.name}</span>
                                        </li>
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                    <div className='showtime'>
                        <div className='title'>
                            <h4>CHỌN SUẤT</h4>
                        </div>
                        <div className='items'>
                            <div>
                                {
                                    showtime.map((n, i) => {
                                        return <div key={i}>
                                            {
                                                n.dates.map((d, i2) => {
                                                    return <div key={i2} className="show">
                                                        <h4>- {d.showDate}</h4>
                                                        <div className='time'>
                                                            {
                                                                d.bundles.map((b) => {
                                                                    return <div>
                                                                        <h5>Ticket {b.version}</h5>
                                                                        <div>
                                                                            {
                                                                                b.sessions.map(t => {
                                                                                    return <span style={{ border: '1px solid black' }} onClick={() => handleBooking(Cinema)}>{t.showTime} </span>
                                                                                })
                                                                            }
                                                                        </div>
                                                                    </div>
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                })
                                            }
                                        </div>
                                    })
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
