import React, { useEffect, useState } from 'react'
import './Film.scss'
import { useParams, useNavigate } from 'react-router-dom'
import Header from '../Header/Header'
import Footer from '../Footer/Footer'




function Film() {
    const { id } = useParams()
    const [film, setFilm] = useState([])
    const [city, setCity] = useState([])
    const [Cinema, setCinema] = useState([])
    const [movies, setMovies] = useState([])
    const navigate = useNavigate();

    useEffect(() => {
        fetch("https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/nowAndSoon")
            .then(res => res.json())
            .then(data => {
                setFilm([
                    ...data.movieShowing.filter((n) => n.id === id),
                    ...data.movieCommingSoon.filter((n) => n.id === id)
                ][0])
            });

        fetch("https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/city")
            .then(res => res.json())
            .then(data => {
                setCity(data)
            });
        fetch("https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/cinemas")
            .then(res => res.json())
            .then(data => {
                setCinema(data)
            });
        fetch("https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/movie/" + id)
            .then(res => res.json())
            .then(data => {
                setMovies(data)
            })
    }, [])

    var curr = new Date();
    curr.setDate(curr.getDate());
    var date = curr.toISOString().substring(0, 10);

    const handleBooking = (Cinema) => {
        navigate(`/login/bookingandticket/${Cinema.id}`)
    }
    {
        return (
            film && <div>
                <Header />
                <div className='films_main'>
                    <div className='films_left'>
                        <div className='films_head'>
                            <div className='films_card' >
                                <div className='films_poster'>
                                    <img src={film.imagePortrait} alt='film_poster' height={400} />
                                </div>
                                <div className='films_detail'>
                                    <h1>{film.name}</h1>
                                    <b className='rate_point'>{Number(film.point).toFixed(1)}<span>/10</span></b>

                                    <h3>Nội Dung Phim</h3>
                                    <div dangerouslySetInnerHTML={{ __html: film.description }}></div>

                                </div>
                            </div>
                        </div>
                        <div className='films_body'>
                            <h3 className='title'>Lịch Chiếu</h3>
                            <div className='showtime' >
                                {
                                    movies.map((n, i) => {
                                        return <div key={i} className='CardCinema'>
                                            <div className="CinemaAll"> Cinema: {n.name}</div>
                                            <div className='Date'>
                                            {
                                                n.dates[0].bundles.map((bundles, i2) => {
                                                    return <div key={i2} className="CardVersion">
                                                        <div className='version' >
                                                            <p>
                                                                {" "}
                                                                {bundles.version} -{" "}
                                                                {bundles.caption ? "Phụ đề" : ""}
                                                            </p>
                                                        </div>
                                                        <div className="cardshowTime">
                                                        {bundles.sessions.map((sessions) => {
                                                            return (
                                                                <div className="showTime" >
                                                                    <div className="BodershowTime">
                                                                        <p>{sessions.showTime}</p>
                                                                    </div>
                                                                </div>
                                                            );
                                                        })}
                                                        </div>
                                                    </div>
                                                })
                                            }

                                            </div>
                                        </div>
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <div className='films_right'>
                              
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}

export default Film