import React, { useEffect, useState } from 'react'
import Footer from '../Footer/Footer'
import Header from '../Header/Header'
import './News.scss'


export default function News() {
  const [BNews, takeNews] = useState([])
  useEffect(() => {
    fetch("https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/moreInfo")
      .then(res => res.json()
        .then(data => takeNews(data)
        ))
  }, [])
  console.log(BNews);
  return (
    <div>
    <Header/>
    <div className='MainBoard'>
      <div className='title'>
        <a><h3>Review</h3></a>
        <div className='Board'>
          {
            BNews.map((n, a) => {
              if (n.type == "review") {
                return <div className='Blog' key={a}>
                  <div  className='imgBlog'> <a style={{ backgroundImage: `url(${n.imageLandscape})` }} /></div>
                  <a className='Eblog' >
                    <div className='inSide'>
                      <h6>{n.name}</h6>
                     <div className='viewrData'>
                      <span className='Viewer'>{n.views}</span>
                      <span className='point'><p>{n.point}</p>/10</span><p className='point'>({n.totalVotes})</p>
                      </div>
                      <span  className='caption' dangerouslySetInnerHTML={{__html: n.shortDescription }}></span> 
                    </div>
                  </a>
                </div>
              }
            })
          }
        </div>
      </div>
      <div className='title'>
        <a><h3>BlogCine</h3></a>
        <div className='Board'>
          {
            BNews.map((n, a) => {
              if (n.type == "promotion") {
                return <div className='Blog' key={a}>
                  <div className='imgBlog'  style={{ backgroundImage: `url(${n.imageLandscape})` }} > </div>
                  <a className='Eblog'>
                    <div className='inSide'>
                      <h6>{n.name}</h6>
                     <div className='viewrData'>
                      <span className='MetaLike'><p>{n.metadata}</p></span>
                      <span className='Viewer'>{n.views}</span>
                      <span className='point'><p>{n.point}</p>/10</span><p className='point'>({n.totalVotes})</p>
                      </div>
                      <span  className='caption'  dangerouslySetInnerHTML={{__html: n.shortDescription }}></span>
                    </div>
                  </a>
                </div>
              }
            })
          }
        </div>
      </div>
    </div>
    <Footer/>
    </div>
  )
}
