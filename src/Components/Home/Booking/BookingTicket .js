import React, { useEffect, useState } from 'react'
import Header from '../Header/Header'
import { useNavigate, useParams } from "react-router-dom"
import _clone from 'lodash/clone';
import './BookingTicket.scss'
import Footer from '../Footer/Footer';

export default function BookingTicket() {
  const [Booking, ManuBooking] = useState([])
  const navigate = useNavigate();
  const [consession, setConsession] = useState([]);
  const [movies, setMovies] = useState([])
  const [sum, setSum] = useState([]);
  const handleContinueteSeatPlan = () => {
    navigate(`/seatPlan`)
}

  useEffect(() => {
    fetch('https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/booking/detail')
      .then(res => res.json()
        .then(data => {
          ManuBooking(data.ticket)
          setConsession(data.consession[0].concessionItems)
        }
        ));

  }, [])

  console.log(Booking);

  const minusQuantity = (index) => {
    let tickets = _clone(Booking)
    if (tickets[index].defaultQuantity == 0) {
      return;
    }
    tickets[index].defaultQuantity -= 1;
    ManuBooking(tickets)
  }
  const addQuantity = (index) => {
    let tickets = _clone(Booking)
    if (tickets[index].defaultQuantity == 8) {
      return;
    }
    tickets[index].defaultQuantity += 1
    ManuBooking(tickets)
  }
  const sumPriceTicket = () => {
    let price = 0;
    Booking.map((tic, index) => {
      price += tic.defaultQuantity * tic.displayPrice
    })
    return price;
  }

  //combo

  const minusQuantityConsession = (index) => {
    let consessions = _clone(consession)
    if (consessions[index].defaultQuantity == 0) {
      return;
    }
    consessions[index].defaultQuantity -= 1;
    setConsession(consessions)
  }
  const addQuantityConsession = (index) => {
    let consessions = _clone(consession)
    if (consessions[index].defaultQuantity == 19) {
      return;
    }
    consessions[index].defaultQuantity += 1
    setConsession(consessions)
  }
  const sumPriceComboConsession = () => {
    let price = 0;
    consession.map((conses, index) => {
      price += conses.defaultQuantity * conses.displayPrice
    })
    return price;
  }

  useEffect(() => {
    // B1 TÍNH TỖNG
    let total = sumPriceTicket() + sumPriceComboConsession();
    setSum(total)

    // B2 TRUYỀN GIÁ TRỊ TOTAL VỀ TRANG CHA LÀ BOOKING TICKET

    // props.passTotalToBookingTicket(total)

    // SAU KHi TRUYỀN DATA VỀ TRANG CHA BOOKING-TICKET, TRANG CHA BẮT DATA ĐÓ, LƯU STATE LẠI, HIỆN THỊ LÊN GIÁ TRỊ TỔNG

  }, [Booking, consession]); //[ticket,consession] có nghĩa có sự thay đổi Hai biến ticket,consession  thì mới chạy vô useEffect hàm này

  return (
    <div>
      <Header />
      <div className='Container'>
        <div className='BTable'>
          <h3>CHỌN VÉ/THỨC ĂN</h3>
          <table className='Ticket' border={1}>
            <thead>
              <tr className='HeadT' >
                <th >Loại vé</th>
                <th style={{ textAlign: 'center' }}>Số lượng</th>
                <th>Giá (VNĐ)</th>
                <th>Tổng (VNĐ)</th>
              </tr>
            </thead>
            <tbody>
              {
                Booking.length > 0 ? Booking.map((n, i) => {
                  return <tr key={i}>
                    <td className='N_D' ><p className='Name'>{n.name}</p><p className='Dct'>{n.description}</p></td>
                    <td className='NSet'>
                      <button onClick={() => minusQuantity(i)}>-</button>
                      <input type='number' value={n.defaultQuantity} disabled />
                      <button onClick={() => addQuantity(i)}>+</button>
                    </td>
                    <td className='Price'>{n.displayPrice}</td>
                    <td className='STotal'>{n.displayPrice * n.defaultQuantity}</td>
                  </tr>
                }) : ""
              }
              <tr className='ATotal'>
                <td>Tổng</td>
                <td>{sumPriceTicket()}</td>
              </tr>
            </tbody>
          </table>
          <table className='ComboTicket' border='1px solid black'>
            <thead>
              <tr className='HeadT'>
                <th >Combo</th>
                <th style={{ textAlign: 'center' }}>Số lượng</th>
                <th>Giá (VNĐ)</th>
                <th>Tổng (VNĐ)</th>
              </tr>
            </thead>
            <tbody>
              {
                consession && consession.length > 0 ? consession.map((m, i) => {
                  return <tr key={i}>
                    <td className='CN_D'>
                      <img src={m.imageUrl} width={100} />
                      <div><p className='Name'>{m.description}</p><p className='Dct'>{m.extendedDescription}</p></div></td>
                    <td className='NSet'>
                      <button onClick={() => minusQuantityConsession(i)}>-</button>
                      <input type='number' value={m.defaultQuantity} disabled />
                      <button onClick={() => addQuantityConsession(i)}>+</button>
                    </td>
                    <td className='Price'>{m.displayPrice}</td>
                    <td className='CTotal'>{m.defaultQuantity * m.displayPrice}</td>
                  </tr>
                }) : ""}
              <tr className='ATotal' >
                <td>tong</td>
                <td>{sumPriceComboConsession()}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className='TotalBil'>
        <div className='TLine'><p>Rap :</p> <h5> </h5> </div>
        <div className='TLine'><p>Suat chieu:</p> <h5> </h5></div>
        <div className='TLine'><p>Ghe:</p></div>
        <div className='TLine'><p>Tong :</p> <h5> </h5> </div>
        <button type="button" className="BT" onClick={handleContinueteSeatPlan} >Tiếp tục</button>
      </div>
      </div>
      <Footer/>
    </div>
  )
}
