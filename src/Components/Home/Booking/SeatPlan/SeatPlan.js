
import Footer from '../../Footer/Footer'
import Header from '../../Header/Header'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import './SeatPlan.scss'
import _clone from 'lodash/clone';

export default function () {
    const [plan, setPlan] = useState({});
    const navigate = useNavigate();
    const handleSeatPlan = () => { navigate(`/bookingandticket`) }
    const handleContinuetePayment = () => {
        navigate(`/login`)
    }
    useEffect(() => {
        fetch("https://teachingserver.onrender.com/cinema/booking/detail")
            .then((res) => res.json())
            .then((data) => {
                setPlan(data.seatPlan.seatLayoutData)
            })
    }, []);

    const handleSeatSelection = (seat, indexArea, indexRow, indexSeat) => {
        let clonePlan = _clone(plan)
        if (!clonePlan.areas[indexArea].rows[indexRow].seats[indexSeat].selected) {
            clonePlan.areas[indexArea].rows[indexRow].seats[indexSeat].selected = true;
            setPlan(clonePlan)
            return;
        }
        if (clonePlan.areas[indexArea].rows[indexRow].seats[indexSeat].selected) {
            clonePlan.areas[indexArea].rows[indexRow].seats[indexSeat].selected = false;
            setPlan(clonePlan)
            return;
        }
    };
    return (
        <div>
            <Header />
            <div className='Container'>
                <div className='TSit' >
                    <h3>Chon Ghe</h3>
                    <div className='Sit-Area'  style={{ width: '100%' }}>
                        {plan.areas && plan.areas.length > 0 ? plan.areas.map((n, indexArea) => {
                            return (

                                <div key={indexArea} >
                                    {n.rows.length > 0 ? n.rows.map((r, indexRow) => {
                                        return (
                                            <div key={indexRow} className='Line-Sit'>
                                                <div className='listSeat'>
                                                    <div className='physicalName '>{r.physicalName}</div>
                                                    <div className='body-seats'>
                                                        {r.seats.length > 0 ? r.seats.map((seat, indexSeat) => {
                                                            return (
                                                                <div key={indexSeat} className="seats">
                                                                    <div className={seat.selected ? "number seat-selected" : "number"} onClick={() => handleSeatSelection(seat, indexArea, indexRow, indexSeat)} >
                                                                        <p>{seat.id}</p>
                                                                    </div>
                                                                </div>
                                                            )
                                                        }) : ""}
                                                    </div>
                                                    <div className='physicalName'>{r.physicalName}</div>
                                                </div>
                                            </div>
                                        )
                                    }) : ""}
                                </div>
                            )
                        }) : ""}


                        <div className='Screen-area'>
                            <h5 className='hint'>Man hinh</h5>
                            <div className='Screen'/>
                        </div>

                         <div className='styleSeat'>
                        <p>
                            <span style={{ background: 'grey' }}></span>
                            Ghế có thể chọn</p>
                        <p>
                            <span style={{ background: 'aqua' }}></span>
                            Ghế đã chọn</p>
                        <p>
                            <span style={{ background: 'lightgreen' }}></span>
                            Ghế đang chọn</p>
                        <p>
                            <span style={{ background: 'red' }}></span>
                            Ghế đã bán</p>
                    </div>
                    </div>

                </div>

                <div className='TotalBil'>
                    <div className='TLine'><p>Rap :</p> <h5> </h5> </div>
                    <div className='TLine'><p>Suat chieu:</p> <h5> </h5></div>
                    <div className='TLine'><p>Ghe:</p></div>
                    <div className='TLine'><p>Tong :</p> <h5> </h5> </div>
                    <button type="button" className="BT" onClick={handleContinuetePayment}>Tiếp tục</button>
                </div>
            </div>
            <Footer />
        </div>
    )
}
