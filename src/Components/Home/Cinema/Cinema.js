
import Footer from '../Footer/Footer'
import Header from '../Header/Header'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import './Cinema.scss'

export default function Cinema() {
    const [cinema, setCinema] = useState([])
    const nav = useNavigate()
    useEffect(()=>{
        fetch("https://vietcpq.name.vn/U2FsdGVkX18VmUfb3R34rkA11q59f+dY6kr6oyXqCbg=/cinema/cinemas")
            .then(res => res.json())
            .then(data =>{
                setCinema(data)
            })
    })

    const getCineName = (slug) =>{
      nav('/cinemas/'+ slug)
    }


  return ( 
    <div>
      <Header/>
      <div className='body'>
        <div className='body-left'>
          <div className='title'>
            <h3 >HỆ THỐNG RẠP</h3>
          </div>
          <div className='AllCinema'>
            {
              cinema.length > 0?cinema.map((n,i)=>{
                return <div key={i} className='CineBox' >
                  <img src={n.imageUrls[0]} alt='CinemaImg' onClick={() => getCineName(n.slug)} />
                  <h4>{n.name}</h4>
                </div>
              }):""
            }
          </div>
        </div>
      </div>
      <Footer/>
    </div>
  )
}
