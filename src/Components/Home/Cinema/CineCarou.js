import React, { useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom'
import Carousel from 'react-bootstrap/Carousel';
import './CineCarou.scss'


function CineCarou() {
    const [Cine, setCineImg] = useState([])
    useEffect(() => {
        fetch("https://vietcpq.name.vn/U2FsdGVkX18VmUfb3R34rkA11q59f+dY6kr6oyXqCbg=/cinema/cinemas")
            .then(res => res.json()
                .then(data =>
                    setCineImg(data)
                ))
                
    }, [])

    return (
     <div>
        <Carousel className='Container'>
        {
            Cine.map((n,i) => {
                return  <Carousel.Item interval={2500} key={i}>
                    <img  className='imgMobile' src={n.imageUrls[0]} width='100%' key={i} alt={n.slug} />
                </Carousel.Item>
            })
        }
        </Carousel>
    </div>
  )}

export default CineCarou

