import React from 'react'
import Footer from '../Footer/Footer'
import Header from '../Header/Header'
import { useState, useEffect } from 'react'
import CineCarou from './CineCarou'
import { useParams } from 'react-router-dom'


export default function InforCinema() {
    const { slug } = useParams
    const [infor, setInfor] = useState([])
    const [cinema, setCinema] = useState([])
    useEffect(() => {
        fetch("https://vietcpq.name.vn/U2FsdGVkX18VmUfb3R34rkA11q59f+dY6kr6oyXqCbg=/cinema/cinemas")
            .then(res => res.json())
            .then(data => {
                setCinema(data)
            })
    }, [])
    useEffect(() => {
        var result = cinema.find((k) => k.slug === slug)
        setInfor(result);
    }, [slug, cinema])

{

    return (
        cinema && <div>
            <Header />
            <div className='body'>
                <CineCarou />
                <div className='body-mid'>
                    <div className='mid-left'>
                        <h3>{cinema.name}</h3>
                        <div dangerouslySetInnerHTML={{ __html: cinema.description }}></div>
                        <p className="address-info"> <span>Địa chỉ: </span> {cinema.address}</p>
                        <p className="address-info"> <span>Phone number: </span>{cinema.phone}</p>
                    </div>
                    <div className='mid-right'>
                        <h3>GIÁ VÉ</h3>
                        {
                            cinema.ticket?.map(v => {
                                return <img alt='fare' src={v.url} className='fare-image' />
                            })
                        }
                        <iframe className="map-embed" src={cinema.mapEmbeb} title='address' />
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}
}
