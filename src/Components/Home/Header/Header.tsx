import './Header.scss'
import { Link } from 'react-router-dom'


export default function Header() {
  return (
    <div className='header'>
      <div className='top-head'>
        <div className='head-logo'>
          <Link to={'/'}> <img  src='https://www.galaxycine.vn/website/images/galaxy-logo.png' alt='logo' /></Link>
        </div>
        <div className='head-bar'>
            <div className='search-box'>
                <input placeholder='Tìm tên phim...' />
                <button className='searchBtn'>
                  <img src='https://www.nicepng.com/png/detail/853-8539483_png-file-search-button-icon-png.png' alt='searchImg' width={20} />
                </button>
            </div>
            <div className='navi-box'>
              <Link to={'/login'}>Đăng nhập</Link>/<Link to={'/register'}>Đăng ký</Link>
            </div>            
        </div>

      </div>
      <div className='bot-head'>
        <nav>
          <ul>
            <li><Link to={'/'}>PHIM</Link></li>
            <li><Link to={'/tickets'}>MUA VÉ</Link></li>
            <li><Link to={'/blogs'}>BLOG ĐIÊN ẢNH</Link></li>
            <li><Link to={'/cinemas'}>RẠP</Link></li>
            <li><Link to={'/members'}>THÀNH VIÊN</Link></li>
          </ul>
        </nav>
      </div>
    </div>
    
  )
}
