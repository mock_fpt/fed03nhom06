import React, { Component } from 'react'
import './Footer.scss'

export default class Footer extends Component {
  render() {
    return (
      <div className='Footer'>
        <div className='LInfo'>
          <div className='F_D' ><h4>INTRODUCE</h4>
          <ul>
            <li><a><p> Về chúng tôi</p></a></li>
            <li><a><p> Thoả thuận sử dụng</p></a></li>
            <li><a><p> Quy chế hoạt động</p></a></li>
            <li><a><p> Chính sách bảo mật</p></a></li>
          </ul>
          </div>
          <div className='F_D'><h4>CINEMA BLOG</h4>
          <ul>
            <li><a><p> Thể loại phim</p></a></li>
            <li><a><p> Bình luận phim</p></a></li>
            <li><a><p> Blog điện ảnh</p></a></li>
            <li><a><p> Phim hay tháng</p></a></li>
          </ul>
          </div>
          <div className='F_D'><h4>SUPPORT</h4>
           <ul>
            <li><a><p> Góp ý</p></a></li>
            <li><a><p> Sale & Services</p></a></li>
            <li><a><p> Rạp / giá vé</p></a></li>
            <li><a><p> Tuyển dụng</p></a></li>
           </ul>
          </div>
          <div><h4>GALAXY CINEMA</h4></div>
        </div>
      </div>
    )
  }
}
