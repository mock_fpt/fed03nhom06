import { call, put, takeEvery } from 'redux-saga/effects'

async function GetAllMovie() {
    var res = await fetch('https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/nowAndSoon')
    var data = await res.json();
    return data;
}

async function GetAllCinemas(){
    var res = await fetch('https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/cinemas')
    var data = await res.json();
    return data;
}

async function GetAllCity(){
    var res = await fetch('https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/city')
    var data = res.json();
    return data
}

async function GetShowtime(id){
    var res = await fetch(`https://vietcpq.name.vn/U2FsdGVkX1953irjJZap3yHVCrEBSJ+JFamfsQiClr4=/cinema/movie/${id}`)
    var data = res.json();
    return data
}

function* getData({type, payload}){
    
    var Moviesdata = yield call(GetAllMovie, payload);
    yield put({type:"GET_MOVIES", payload: Moviesdata})
}

function* getCinemas({type, payload}){
    var Cinemasdata = yield call(GetAllCinemas, payload);
    yield put({type:"GET_CINEMAS", payload: Cinemasdata})
}

function* getCity({type,payload}){
    var Citydata = yield call(GetAllCity,payload);
    yield put({type:"GET_CITY", payload: Citydata})
    console.log(payload);
}

function* mySaga(){
    yield takeEvery("GET_MOVIES", getData)
    yield takeEvery("GET_CINEMAS", getCinemas)
    yield takeEvery("GET_CITY", getCity)
}

export default mySaga;