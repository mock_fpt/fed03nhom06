const intialState = {
    City: []
}

const CityRdc = (state = intialState, { type,payload }) => {
    
    switch (type) {
        case "GET_CITY":
            return {City: payload }
        default:
            return state
    }
}

export default CityRdc;