const intialState = {
    ListMovieS:[],
    ListMovieCS:[]
}

const MovieRdc = (state = intialState, { type,payload }) => {
    
    switch (type) {
        case "GET_MOVIES":
            return { 
                ...state,
                ListMovieS:payload.movieShowing,
                ListMovieCS:payload.movieCommingSoon
            }
        default:
            return state
    }
}

export default MovieRdc;