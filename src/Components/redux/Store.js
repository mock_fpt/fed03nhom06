import MovieRdc from "./reducer/MovieRdc";
import CinemasRdc from "./reducer/CinemasRdc";
import createSagaMiddleware from 'redux-saga'
import mySaga from "./SagaLsM/ApiSaga";
import CityRdc from "./reducer/CityRdc";


const sagaMiddleware = createSagaMiddleware()

var redux = require("redux")

const allReducer = redux.combineReducers({
    MovieSet:MovieRdc,
    CinemaSet: CinemasRdc,
    CitySet: CityRdc
})

export default redux.createStore(allReducer, redux.applyMiddleware(sagaMiddleware))

sagaMiddleware.run(mySaga)


