import { BrowserRouter, Route, Routes } from 'react-router-dom'
import ManuFilm from './Components/Home/Body/ManuFilm/ManuFilm'
import ManuFilm2 from './Components/Home/Body/ManuFilm/ManuFilm2'
import BookingTicket from './Components/Home/Booking/BookingTicket '
import SeatPlan from './Components/Home/Booking/SeatPlan/SeatPlan'
import Cinema from './Components/Home/Cinema/Cinema'
import InforCinema from './Components/Home/Cinema/InforCinema'
import News from './Components/Home/CineNews/News'
import Film from './Components/Home/Films/Film'
import Home from './Components/Home/Home'
import TicketByCinema from './Components/Home/Ticket/TicketByCinema'
import TicketByMovie from './Components/Home/Ticket/TicketByMovie'
import Tickets from './Components/Home/Ticket/Tickets'
import Login from './Components/Login/Login'
import Register from './Components/Login/Register'




export default function App() {
  
  
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home />} >
            <Route>
              <Route path='movieShowing' element={<ManuFilm/>}  />
              <Route path='movieComming' element={<ManuFilm2/>}  />
            </Route>
          </Route>
          <Route path='/:id' element={<Film/>}/>
          <Route path='/login' element={<Login/>}>
          </Route>
          <Route path='/login/bookingandticket/:id' element={<BookingTicket/>} />
          <Route path='/login' element={<Login/>}>  </Route>
          <Route path='/bookingandticket/:id' element={<BookingTicket/>}></Route>
         
          

          <Route path='/register' element={<Register/>}/>
          <Route path='/tickets' element={<Tickets/>} >
              <Route path='TicketByMovie' element={<TicketByMovie/>} />
              <Route path='TicketByCinema' element={<TicketByCinema/>} />
          </Route>
          <Route path='/cinemas' element={<Cinema/>}/>
          <Route path='/cinemas/:slug' element={<InforCinema/>} />
          <Route path='/blogs' element={<News/>} />
          <Route path='/seatplan'element={<SeatPlan/>}/>
        </Routes>
      </BrowserRouter>
    </>
  )
}
